const Config = {
    maxIp: process.env.MAX_REQUEST_BY_IP | 20,
    maxPath: process.env.MAX_REQUEST_BY_PATH | 5
}

module.exports = Config