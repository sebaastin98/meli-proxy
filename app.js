require("dotenv").config();
const express = require("express");
const cors = require("cors");

const setRoutes = require('./app/routes/routes')

const PORT = 5000

const app = express();

app.use(cors());

app.set('trust proxy', true);

setRoutes(app)


app.listen(PORT, () => {
    console.log('Servidor corriendo en el puerto ', PORT);
});
