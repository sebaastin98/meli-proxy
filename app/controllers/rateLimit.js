const axios = require("axios");
const Config = require("../../Config.js");
const API_URL = process.env.API_URL
const client = require('./../repositories/redis.js')

const rateLimitControl = (req, res) => {

    client.get(req.ip, async (err, reply) => {
    
        if (err) return res.status(500).send('Hubo un error en el servidor.')

        let ipData = JSON.parse(reply)

        if (!ipData) {

            ipData = {
                ip: req.ip,
                count: 0,
                path: [{
                    name: req.originalUrl,
                    count: 0
                }]
            }

        }
        let path = ipData.path.find(item => item.name == req.originalUrl);
        if (!path) {
            ipData.path.push({
                name: req.originalUrl,
                count: 0
            })
        }

        if (ipData.count >= Config.maxIp || path && path.count >= Config.maxPath) {

            res.status(429).send('Ha alcanzado el límite de solicitudes.')

        } else {

            const uriApi = API_URL + req.originalUrl;
            const response = await axios.get(uriApi);
            res.status(response.status).send(response.data)

            if (response.status == 200) {

                ipData.count++
                let itemIndex = ipData.path.findIndex((item => item.name == req.originalUrl));
                ipData.path[itemIndex].count++
                client.set(req.ip, JSON.stringify(ipData))

            }

        }
    })
}

module.exports = rateLimitControl