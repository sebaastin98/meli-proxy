const rateLimit = require('./../controllers/rateLimit')

const setRoutes = (app) => {
    app.get('/*', (req, res) => {
        try {
            rateLimit(req, res)
           
        } catch (error) {
    
            console.log(error);
            res.status(500).send(error.message);
        }
    
    });
}

module.exports = setRoutes

